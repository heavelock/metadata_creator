from bs4 import BeautifulSoup
import scipy.io
import os
import obspy
from datetime import datetime, timedelta


# Parsowanie pliku xml z metadanymi
def parse_event_names(file):
    soup = BeautifulSoup(open(file), 'lxml-xml')
    parsed_meta = {}
    for ev in soup.find_all('event'):
        parsed_meta[str(ev.file.contents[0])] = ev['id']
    return parsed_meta


# parsowanie katalogu
def parse_catalog(file):
    mat = scipy.io.loadmat(file)

    for key in mat.keys():
        if key[0:2] != '__':
            break

    cata_len = len(mat[key][0][0][2])
    catalog = {}

    for subcat in mat[key][0]:
        catalog[subcat[0][0].lower()] = [subcat[2][i][0] for i in range(cata_len)]

    return catalog

def write_metadata_file(meta_file, description, starttime, endtime, event_id):
    # Zapis do pliku
    meta_file.write('<metadata>' + "\n")
    meta_file.write('  <item name="episodeName" isRequired="true" minLength="10"/>' + "\n")
    meta_file.write('  <item name="episodeCode" isRequired="true" minLength="2"/>' + "\n")
    meta_file.write(
        '  <item name="episodeOwner" isRequired="true" enum="[&quot;IGF PAS&quot;, &quot;KWSA&quot;, &quot;GIG&quot;,&quot;GFZ&quot;, &quot;VAST&quot;]"/>' + "\n")
    meta_file.write(description + "\n")
    meta_file.write('  <item name="text" isRequired="true"/>' + "\n")
    meta_file.write(
        '  <item name="country" isRequired="true" enum="[&quot;Poland&quot;, &quot;Germany&quot;, &quot;Vietnam&quot;]"/>' + "\n")
    meta_file.write(
        '  <item name="region" enum="[&quot;Upper Silesia&quot;, &quot;Gross Schoenebeck&quot;, &quot;Legnica-Glogow Copper District&quot;, &quot;Niedzica&quot;, &quot;Quang Nam Province&quot;]"/>' + "\n")
    meta_file.write(
        '  <item name="positionType" isRequired="true" enum="[&quot;point&quot;, &quot;polygon&quot;]"/>' + "\n")
    meta_file.write(
        '  <item name="coordinateSystem" isRequired="true" enum="[&quot;LOCAL:POL001&quot;, &quot;WGS-84&quot;]"/>' + "\n")
    meta_file.write('  <item name="longitude" isRequired="true"/>' + "\n")
    meta_file.write('  <item name="latitude" isRequired="true"/>' + "\n")
    meta_file.write(starttime + "\n")
    meta_file.write(endtime + "\n")
    meta_file.write(event_id + "\n")
    meta_file.write('  <item name="itemType" readOnly="true"/>' + "\n")
    meta_file.write('  <item name="name" readOnly="true"/>' + "\n")
    meta_file.write('  <item name="path" readOnly="true"/>' + "\n")
    meta_file.write(
        '  <item name="dataType" enum="[&quot; &quot;, &quot;catalog&quot;, &quot;ground motion catalog&quot;, &quot;waveform mseed&quot;, &quot;waveform seed&quot;, &quot;signal seed&quot;, &quot;signal mseed&quot;, &quot;signal seed accelerogram&quot;, &quot;signal mseed accelerogram&quot;, &quot;waveform seed accelerogram&quot;, &quot;waveform mseed accelerogram&quot;, &quot;dataless&quot;, &quot;ray tracing angles&quot;, &quot;seismic network&quot;, &quot;ground motion network&quot;, &quot;water level&quot;, &quot;water volume&quot;, &quot;injection pressure&quot;, &quot;injection rate&quot;, &quot;well path&quot;, &quot;mining front advance&quot;, &quot;mining polygon advance&quot;, &quot;bathymetry&quot;, &quot;shoreline&quot;, &quot;exploitation boundary&quot;, &quot;mine area&quot;, &quot;mine map&quot;, &quot;wellhead pressure&quot;, &quot;tectonics&quot;, &quot;stratigraphy&quot;, &quot;velocity model&quot;, &quot;rock parameters&quot;, &quot;shear modulus&quot;, &quot;hydrogeology&quot;, &quot;topography&quot;, &quot;shear wave velocity&quot;, &quot;geotiff&quot;, &quot;episode logo&quot;, &quot;episode image&quot;, &quot;episode xml&quot;, &quot;shp file&quot;]"/>' + "\n")
    meta_file.write(
        '  <item name="impactingFactor" isRequired="true" enum="[&quot;underground mining&quot;, &quot;geothermal energy production&quot;, &quot;reservoir impoundment&quot;]"/>' + "\n")
    meta_file.write(
        '  <item name="type" enum="[&quot;data relevant for the considered hazards&quot;, &quot;seismic&quot;, &quot;water quality&quot;, &quot;air quality&quot;, &quot;satellite&quot;, &quot;industrial&quot;, &quot;geodata&quot;]"/>' + "\n")
    meta_file.write('  <item name="auxiliary" enum="[&quot;1&quot;]"/>' + "\n")
    meta_file.write(
        '  <item name="allowedDownload" isRequired="true" enum="[&quot;SHEER&quot;, &quot;EPOS-IP&quot;, &quot;all&quot;, &quot;affiliated&quot;]"/>' + "\n")
    meta_file.write(
        '  <item name="allowedVisibility" isRequired="true" enum="[&quot;SHEER&quot;, &quot;EPOS-IP&quot;, &quot;all&quot;, &quot;affiliated&quot;]"/>' + "\n")
    meta_file.write('  <item name="dataPolicy" isRequired="true"/>' + "\n")
    meta_file.write('</metadata>' + "\n")

if __name__ == "__main__":

    ## Wszystkie inputy za jednym razem
    # Podaj sciezke do katalogu roboczego
    working_directory = input('Podaj sciezke gdzie znajduja sie wszystkie pliki.' + '\n')

    # Sprawdzenie czy podana lokalizacja folderu roboczego istnieje, jesli nie, próba wykonania skryptu w obecnej lokalizacji
    if os.path.isdir(working_directory):
        # Zmiana folderu roboczego
        os.chdir(working_directory)
    else:
        # Wypisz w konsoli komunikat
        print('Podana lokalizacja nie jest poprawna, sprobuje wykonac skrypt w lokalizacji gdzie znajdujde sie plik skryptu.')

    # Podaj numer pierwszego sygnalu
    first_signal_no = input('Podaj numer pierwszego sygnalu.' + '\n')

    # Jesli numer pominiety to zostaje przypisane mu zero, jesli cos wpisane to nastepuje walidacja
    if first_signal_no == '':
        first_signal_no = 0
    else:
        first_signal_no = int(first_signal_no)

    # Podaj numer ostatniego sygnalu
    last_signal_no = input('Podaj numer ostatniego sygnalu.' + '\n')
    # Jesli numer pominiety to zostaje przypisane mu 99999999, jesli cos wpisane to nastepuje walidacja
    if last_signal_no == '':
        last_signal_no = 9999999
    else:
        last_signal_no = int(last_signal_no)

    # Prealokacja zmiennej dla xmla
    xml_name = ''
    i = 0
    # Petla prawdzajaca czy podany plik xml istnieje
    while not os.path.exists(xml_name):
        # Podaj nazwe xmla
        xml_name = input('Podaj pelna nazwe pliku xml.' + '\n')
        # Jesli podanie nazwy zostaje pominiete, znajdz plik w folderze roboczym.
        if xml_name == '':
            xml_name = [f for f in os.listdir() if f[-4:] == '.xml'][0]
        i += 1
        if i == 10:
            raise ValueError(
                'Nie udalo Ci sie podac poprawnej nazwy XMLa az 10 razy, w podanym folderze tez nie ma tego pliku.'
                ' Sprawdz czy wszystko jest ok.')

    #Prealokacja zmiennej dla katalogu
    catalog_name = ''
    i = 0
    # Petla sprawdzajaca czy podany plik katalogu istnieje
    while not os.path.exists(catalog_name):
        # Podaj nazwe piku z katalogiem
        catalog_name = input('Podaj pelna nazwe pliku z katalogiem.' + '\n')

        # Jesli podanie nazwy zostalo pominiete, znajdz plik w folderze roboczym
        if catalog_name == '':
            catalog_name = [f for f in os.listdir() if f[-4:] == '.mat'][0]
        i += 1
        if i == 10:
            raise ValueError(
                'Nie udalo Ci sie podac poprawnej nazwy katalogu az 10 razy, w podanym folderze tez nie ma tego pliku.'
                ' Sprawdz czy wszystko jest ok.')

    # Podaj nazwe publikacji w CIBIS
    try:
        cibis_publication_no = int(input('Podaj numer publikacji CIBIS.' + '\n'))
    except ValueError:
        print('Podana wartosc nie jest liczba.')

    #Prealokacja zmiennej dla typu magnitudy
    magnitude_type = None
    #Petla sprawdzajaca czy typ magnitudy jest poprawnie wpisanyw
    while not magnitude_type in ('ml','mw'):
        magnitude_type = input('Wykorzystac mw czy ml? Wpisz mw lub ml.' + '\n')


    # Parsowanie catalogu
    parsed_catalog = parse_catalog(catalog_name)
    # Parsowanie xmla z nazwami
    event_names = parse_event_names(xml_name)

    # Lista plikow seed w katalogu, zawężona do plikow o numerach wiekszych lub rownych numerowi poczatkowemu i
    #  mniejszych lub rownych numerowi koncowemu
    file_list = [f for f in os.listdir() if f[-5:] == '.seed' and
                 int(f[-11:-5]) >= first_signal_no and
                 int(f[-11:-5]) <= last_signal_no]

    for file in file_list:
        # Przeczytaj plik seed
        seed_file = obspy.read(file)
        # Tworzenie pliku metadanych
        metadata_file = open(''.join([file, '_', str(cibis_publication_no), '.metadata']), mode='w')
        # Uzyskanie earthquake id z XMLa
        earthquake_id = event_names[file]
        # Pobierz magnitude z katalogu
        magnitude = parsed_catalog[magnitude_type][parsed_catalog['id'].index(earthquake_id)]
        # Pobierz date z katalogu, format matlabowy
        matlab_datenum = parsed_catalog['time'][parsed_catalog['id'].index(earthquake_id)]
        # Skonwertuj date z formatu matlabowego na pythonowy
        date_of_eq =  datetime.fromordinal(int(matlab_datenum)) + timedelta(days=matlab_datenum%1) - timedelta(days = 366)

        if magnitude_type == 'ml':
            description_magnitude_type = ' with ML='
        else:
            description_magnitude_type = ' with Mw='

        # Stworz linijke description
        description = ''.join(['  <item name="description">','Signal of event which occurred on ',
                               date_of_eq.strftime('%Y-%b-%d %X'), description_magnitude_type,
                               str(magnitude), ' (eventID=', earthquake_id, ')', '</item>'])
        # Stworz linijke starttime
        starttime = ''.join(['  <item name="start">',seed_file[0].stats.starttime.strftime('%Y%m%d%H%M%S.3Z'),'</item>'])
        # Stworz linijke endtime
        endtime = ''.join(['  <item name="end">', seed_file[0].stats.endtime.strftime('%Y%m%d%H%M%S.3Z'), '</item>'])
        # Stworz linijke event_id
        event_id = ''.join(['  <item name="eventID">',earthquake_id,'</item>'])

        # Zapisz wszystko do pliku
        write_metadata_file(metadata_file,description,starttime,endtime,event_id)

        #Zamknij plik
        metadata_file.close()

input('Udalo sie! Wcisnij enter aby zamknac okno.')


